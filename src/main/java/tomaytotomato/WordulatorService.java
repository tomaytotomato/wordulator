package tomaytotomato;

import java.util.LinkedHashMap;
import java.util.List;

public interface WordulatorService {

  /**
   * Returns a map of count for each occurrence of a word in a list of string inputs
   * @param wordList list
   * @return map
   */
  LinkedHashMap<String, Integer> wordulate(List<String> wordList);

}
