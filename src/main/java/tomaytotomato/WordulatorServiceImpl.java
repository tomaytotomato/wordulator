package tomaytotomato;

import static java.util.stream.Collectors.toMap;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;

public class WordulatorServiceImpl implements WordulatorService {

  /**
   * Iterate through list of words and add them to map, increment each word if they already exist.
   *
   * Then sort first by key to get a first assortment of alphabeticalness, then sort by value.
   * @param wordList list
   * @return map
   */
  @Override
  public LinkedHashMap<String, Integer> wordulate(final List<String> wordList) {

    final HashMap<String, Integer> wordCountMap = new HashMap<>();

    wordList.stream()
        .map(StringUtils::trimToEmpty)
        .filter(StringUtils::isNotBlank)
        .filter(StringUtils::isAlpha)
        .map(StringUtils::lowerCase)
        .forEach(word -> {
          wordCountMap.merge(word, 1, Integer::sum);
        });

    final Map<String, Integer> sortedMapByAlpha = new TreeMap<>(wordCountMap);

    return sortedMapByAlpha.entrySet().stream()
        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
        .collect(
            toMap(Map.Entry::getKey, Map.Entry::getValue, (key, value) -> value,
                LinkedHashMap::new));
  }
}
