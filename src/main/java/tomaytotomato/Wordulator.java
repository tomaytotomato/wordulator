package tomaytotomato;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.kohsuke.args4j.CmdLineException;
import tomaytotomato.util.CommandOptions;

/**
 * Wordulator, parses a file with words and calculates the occurrence of each word in that input.
 */
public class Wordulator {

  public static void main(final String[] args) {

    final WordulatorService wordulatorService = new WordulatorServiceImpl();

    try {
      final CommandOptions cli = new CommandOptions(args);

      if (StringUtils.isNotBlank(cli.getInputPath())) {
        System.out.println("Reading from file : " + cli.getInputPath());

        try (Stream<String> stream = Files.lines(Paths.get(cli.getInputPath()))) {

          final LinkedHashMap<String, Integer> result =
              wordulatorService.wordulate(stream.collect(Collectors.toList()));

          for (final Map.Entry<String, Integer> stringIntegerEntry : result.entrySet()) {
            System.out.println(((Map.Entry) stringIntegerEntry).getKey() + " : "
                + ((Map.Entry) stringIntegerEntry).getValue());
          }

        } catch (IOException e) {
          e.printStackTrace();
        }

      } else {
        System.err.println("No input file specified, make sure to use the -i operator e.g. -i <path to file>");
      }

    } catch (IOException | CmdLineException e) {
      System.err.println("Error parsing program arguments");
    }
  }
}
