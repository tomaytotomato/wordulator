package tomaytotomato.util;

import java.io.IOException;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

/**
 * Supported CLI operations
 */
public class CommandOptions {

    @Option(name = "-i", aliases = "--input", usage = "input from this file")
    private String inputPath;

    /**
     * Constructor takes in args from main
     *
     * @param args String[]
     */
    public CommandOptions(final String[] args) throws IOException, CmdLineException {
        final CmdLineParser parser = new CmdLineParser(this);
        parser.parseArgument(args);
    }

    public String getInputPath() {
        return inputPath;
    }
}
