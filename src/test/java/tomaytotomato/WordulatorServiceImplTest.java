package tomaytotomato;

import static org.junit.Assert.assertEquals;

import com.google.common.collect.ImmutableList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class WordulatorServiceImplTest {

  private WordulatorService wordulatorService;

  @Before
  public void setUp() throws Exception {
    wordulatorService = new WordulatorServiceImpl();
  }

  @Test
  public void wordulateCorrectCount() {

    //note ordering of keys by alphabetical order
    final LinkedHashMap<String, Integer> expected = new LinkedHashMap<String, Integer>() {{
      put("hello", 3);
      put("foo", 2);
      put("bobby", 1);
      put("tables", 1);
    }};

    System.out.println("Expected : " + expected.keySet());

    final List<String> input = ImmutableList.of("hello", "foo", "hello", "tables", "hello", "bobby", "foo");
    final LinkedHashMap<String, Integer> result = wordulatorService.wordulate(input);

    System.out.println("Result : " + result.keySet());

    assertEquals(expected.size(), result.size());

    final Iterator expectedIterator = expected.keySet().iterator();
    final Iterator resultIterator = result.keySet().iterator();

    while (expectedIterator.hasNext() && resultIterator.hasNext()) {
      assertEquals(expectedIterator.next(), resultIterator.next());
    }
  }

  @Test
  public void wordulateCorrectFlippedInputList() {

    //note ordering of keys by alphabetical order
    final LinkedHashMap<String, Integer> expected = new LinkedHashMap<String, Integer>() {{
      put("bar", 3);
      put("foo", 2);
      put("womp", 1);
    }};

    System.out.println("Expected : " + expected.keySet());

    final List<String> input1 = ImmutableList.of("bar", "foo", "foo", "bar", "womp", "bar");
    final List<String> input2 = ImmutableList.of("bar", "womp", "bar", "bar", "foo", "foo");


    final Iterator expectedIterator = wordulatorService.wordulate(input1).keySet().iterator();
    final Iterator resultIterator = wordulatorService.wordulate(input2).keySet().iterator();

    while (expectedIterator.hasNext() && resultIterator.hasNext()) {
      assertEquals(expectedIterator.next(), resultIterator.next());
    }
  }

  /**
   * Blanks is number
   */
  @Test
  public void wordulateCorrectIgnoreNonWords() {
    //note ordering of keys by alphabetical order
    final LinkedHashMap<String, Integer> expected = new LinkedHashMap<String, Integer>() {{
      put("mirage", 3);
      put("tornado", 2);
      put("typhoon", 1);
    }};

    System.out.println("Expected : " + expected.keySet());

    final List<String> input = ImmutableList.of("tornado", "12", "mirage", "mirage", "typhoon", "5120951.24", "tornado");
    final LinkedHashMap<String, Integer> result = wordulatorService.wordulate(input);

    System.out.println("Result : " + result.keySet());

    assertEquals(expected.size(), result.size());

    final Iterator expectedIterator = expected.keySet().iterator();
    final Iterator resultIterator = result.keySet().iterator();

    while (expectedIterator.hasNext() && resultIterator.hasNext()) {
      assertEquals(expectedIterator.next(), resultIterator.next());
    }
  }

  @Test
  public void wordulateHandleEmptyInputList() {
    //note ordering of keys by alphabetical order
    final LinkedHashMap<String, Integer> expected = new LinkedHashMap<String, Integer>() {{
    }};

    System.out.println("Expected : " + expected.keySet());

    final List<String> input = ImmutableList.of();
    final LinkedHashMap<String, Integer> result = wordulatorService.wordulate(input);

    System.out.println("Result : " + result.keySet());

    assertEquals(expected.size(), result.size());

    final Iterator expectedIterator = expected.keySet().iterator();
    final Iterator resultIterator = result.keySet().iterator();

    while (expectedIterator.hasNext() && resultIterator.hasNext()) {
      assertEquals(expectedIterator.next(), resultIterator.next());
    }

  }

  @Test
  public void wordulateNormaliseInputList() {

    //note ordering of keys by alphabetical order
    final LinkedHashMap<String, Integer> expected = new LinkedHashMap<String, Integer>() {{
      put("jam", 3);
      put("tip", 2);
    }};

    System.out.println("Expected : " + expected.keySet());

    final List<String> input = ImmutableList.of("JAM", "TIP         ", "JAM", "    TIP", "JAM");
    final LinkedHashMap<String, Integer> result = wordulatorService.wordulate(input);

    System.out.println("Result : " + result.keySet());

    assertEquals(expected.size(), result.size());

    final Iterator expectedIterator = expected.keySet().iterator();
    final Iterator resultIterator = result.keySet().iterator();

    while (expectedIterator.hasNext() && resultIterator.hasNext()) {
      assertEquals(expectedIterator.next(), resultIterator.next());
    }
  }
}