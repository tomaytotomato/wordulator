Wordulator
---

Command line application that reads in a text file and prints 
the count of each occurrence of a word in the file, in descending order.



### Input

One word for each line

    foo.txt
    
    
    hello 
    Steve 
    Ballmer
    Windows 
    is 
    hello
    Mark
    open 
    source
    hello 
    Steve 
    ...
    
### Output

    hello: 3
    Windows: 1
    ....etc
    

### Edge Cases

* File not found / permissions
* Empty files
* Non alpha characters
* Hyphenated or incorrect words
* Mixed case characters
* Words with same count, sort by alphabetical?

### Solutions

You could do it in Unix with command line tools that exist, such as translate.

    < foo.txt tr -sc '[:alpha:]' '[\n*]' | sort | uniq -c | sort -nr
    
from [StackOverflow](https://unix.stackexchange.com/questions/39039/get-text-file-word-occurrence-count-of-all-words-print-output-sorted)


### Pseudocode

1. Get input lines
2. Normalise each line (lowercase, trim whitespace, remove numbers etc.)
3. Create Map with key of String and Value of Integer 
4. Iterate through each line
    - If word doesn't exist in Map, add it
    - Else increment its counter
5. When finished, sort hashmap by value


### Dependencies

* Google Guava library, lots of helper classes that makes life easier
* Args4j, older library but works and saves having to roll your own CLI parser.
* Apache Lang Commons 3 for handy string methods